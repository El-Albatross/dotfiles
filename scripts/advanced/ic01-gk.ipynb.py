## INST 126 – Introduction to Programming for Information Science

## In-Class Exercise 1: Variables and Conditionals
#### Submission (via ELMS) - Work in pairs. One member of your pair will submit a completed copy of your Jupyter notebook containing your programs and responses to the questions below. The exercise is listed as `IC-01`. If you do not submit with both names, neither person will get credit.
#Team Names: Montserrat Carrillo Ade, Gabe Kamalakis
### Exercise 1 
##### Let’s practice debugging code! Look at each of the following code segments, and without running each cell block, determine whether or not they would run. If they would run, comment (#) the solution. If the code would not run, comment, on the same line as the errors, what you would do to correct any errors. Afterwards, run the cell block to see if you response was correct. 

first_variable = 3 # changed value from string to an integer

second_variable = 10

result = (first_variable + second_variable)

print(result)



# Would it run? _____no__

# If yes, what is the output? _____

# If no, correct the error end explain what you did on the same line as the error



user_input = input("What is your name") # turned value into a string

print("My name is " + user_input)



# Would it run? ___no____

# If yes, what is the output? _____

# If no, correct the error end explain what you did on the same line as the error 

current_year = 2019

birth_year = int(input("What is your birth year?")) #converted input to integer

age = current_year - birth_year

number = str(age) # changed aged to a string

print("You will turn " + number + " this year (if you haven't already).")



# Would it run? _____no__

# If yes, what is the output? ____

# If no, correct the error end explain what you did on the same line as the error 

favorite_number = 2

print("My favorite number is " + str(favorite_number) + "!") # added a plus sign



# Would it run? ___no____

# If yes, what is the output? _____

# If no, correct the error end explain what you did on the same line as the error 

### Exercise 2
##### Getting even more input. Ask the user for the day of the week, using the input() function, and then print a sentence that includes the current day of the week in it. When you run your program it should look something like this:

#

#Please enter the day of the week: Wednesday

#

#Today is Wednesday!

#

# Use the input function to ask the user to enter the day of the week 

user_input = input("Please enter the day of the week: ") 



# Display a sentence with the user input 

print("Today is "+ user_input + "!") 

### Exercise 3
##### Getting even more input. Ask the user for their first and last names, using the input() function, and then print a sentence that includes their name in it. When you run your program it should look something like this:
# - Prompt the user. Prompt the user for their first name, and store the value in a variable called `first_name`.

# - Prompt the user. Prompt the user for their last name, and store the value in a variable called `last_name`.

# - Concatenate the names. Combine the first and last names, and assign the value to a variable, `full_name`. Be sure to put a space between their first and last name. 

# - Display a greeting. Print a greeting that uses their full name, producing a complete sentence. Don’t forget to provide the ending punctuation.

#

# 
first_name = input("Whats your first name?")

last_name = input("What is your last name? ") 



full_name  = first_name + " " + last_name

print("Hello, " + full_name + "!") 
### Exercise 4

#

#      

#    
##### A program was created that outputs the following unto the console: 
#`Hello! What is your name:` 

#

#`Hello, Jane Doe! Using this program, we can help you determine you pay for the week!` 

#

#`Please enter the hours you worked this week: 20`

#

#`Next, please enter your rate of pay: 10`

#

#`Calculating… you will make $ 200.00 by the end of the week!`
##### Unfortunately, the program is lost and cannot not be retrieved. One team had the following framework but could not complete the rest of the program. Fill in the blanks and test that your program is correct! 
user_input = input("Hello! What is your name:")



#Write the same greeting above with the user’s name

print("Hello, "+user_input+"! Using this program, we can help you determine you pay for the week!")



hours_worked = int(input("Please enter the hours you worked this week: "))



#Ask the user for the rate of pay following the same format from the output above

rate_of_pay = int(input("Next, please enter your rate of pay: "))



#Create a variable that multiplies the hours_worked and the rate_of_pay

pay_for_week= hours_worked*rate_of_pay



#Write a print statement that will output the last line of the output provided

print("Calculating… you will make $" + str(pay_for_week) + " by the end of the week!")

### Exercise 5
##### Create one coherent program that contains each of the following errors. Underneath this incorrect code, write the corrected version and insert comments that explain the corrections you made

# Inappropriate use of the input() function 

user_input  = input(blah)

# Use of an illegal variable 

43 = input("^")
# Incorrectly concatenating different data types 

"titties" + 5 
# Any type of syntax error in the print statement 

print(booty cheeks)
### Exercise 6

##### Fill in the blanks using the following operators to produce to achieve the desired output. 

# To produce the output: 163

question_1 = ( 12 ** 2) + 19 

print(question_1)



# To produce the ouput: 0 

question_2 = 12 // 2 - 3 * 2

print(question_2)



# To produce the output: 16

question_3 = ( 2 + 2 ) * 5 - 4

print(question_3)

### Exercise 7

#

##### Let's practice using a simple conditional statement! Write a simple "bouncer" program that will prompt for the user's age, and tell them whether they can come into a bar or not, depending on how old they are.

#

#If correctly running, it should look something like this:

#

#Hello, potential customer! How old are you? 21

#Come on in!

#

#OR:

#

#Hello, potential customer! How old are you? 19

#Sorry, you need to be at least 21 to come in.
# Write your code here

age= int(input('Hello, potential customer! How old are you?'))



if age < 21:

        print(str(age) + " "+ "Sorry, you need to be at least 21 to come in")

else:

    print('Come on in!')

### Exercise 8
##### Let's modify our program from Exercise 4 to comply with overtime rules. Let's give the employee 1.5 times the hourly rate for hours worked above 40 hours. Hint: use conditionals!

#

#For example, if the number of hours worked is 45, and the rate of pay is 10, you should get 475.0 (10/hr for 40 hrs, and 15/hr for 5 hrs).
# Copy/paste your solution from Exercise 4 here, and modify it.

user_input = input("Hello! What is your name:")



#Write the same greeting above with the user’s name

print("Hello, "+user_input+"! Using this program, we can help you determine you pay for the week!")



hours_worked = int(input("Please enter the hours you worked this week: "))



#Ask the user for the rate of pay following the same format from the output above

rate_of_pay = int(input("Next, please enter your rate of pay: "))



if hours_worked >= 40:

    overtime = hours_worked - 40 

    pay_for_week= 40*rate_of_pay + (overtime * 1.5 * rate_of_pay) 

    print(overtime)

else:

    pay_for_week= hours_worked*rate_of_pay

#Create a variable that multiplies the hours_worked and the rate_of_pay





#Write a print statement that will output the last line of the output provided

print("Calculating… you will make $" + str(pay_for_week) + " by the end of the week!")

### Exercise 9 (optional)

#

##### Now let's make our program even better! Sometimes the user might put in non-numeric input by mistake (for hours worked or hourly rate), which will cause a ValueError exception. Let's practice using a try/except to exit the program gracefully and give us an informative error so the user knows what actually went wrong.
# Copy/paste your solution from Exercise 4 here, and modify it.

user_input = input("Hello! What is your name:")



#Write the same greeting above with the user’s name

print("Hello, "+user_input+"! Using this program, we can help you determine you pay for the week!")



hours_worked = int(input("Please enter the hours you worked this week: "))



#Ask the user for the rate of pay following the same format from the output above

rate_of_pay = int(input("Next, please enter your rate of pay: "))



if hours_worked >= 40:

    overtime = hours_worked - 40 

    pay_for_week= 40*rate_of_pay + (overtime * 1.5 * rate_of_pay) 

    print(overtime)

else:

    pay_for_week= hours_worked*rate_of_pay

#Create a variable that multiplies the hours_worked and the rate_of_pay





#Write a print statement that will output the last line of the output provided

print("Calculating… you will make $" + str(pay_for_week) + " by the end of the week!")

