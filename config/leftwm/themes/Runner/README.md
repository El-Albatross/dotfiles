# Runner
A moody, but vibrant theme centered around modified Tokyo Night colors.

## Screenshots

## Dependencies
- leftwm
- polybar
- nitrogen
- rofi _(optional)_
- dunst _(optional)_

## Installing
1. `git clone https://gitlab.com/omarkamalakis/runner.git`
2. `ln -s $PWD/Runner $HOME/.config/leftwm/themes/current`
3. Reload LeftWM
