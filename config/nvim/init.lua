-- VIMRC -> Init.vim -> Init.LUA

--------------------------------
--      GENERAL SETTINGS
--------------------------------
local opt = vim.opt

vim.g.mapleader = " "

opt.hlsearch = true -- highlight search
opt.mouse = "a" -- make mouse work
opt.clipboard = "unnamedplus"
opt.ttyfast = true
opt.relativenumber = true
opt.number = true
opt.completeopt = "menu,menuone,noselect"
opt.wildmode = "longest,list,full"
opt.tabstop = 4
opt.signcolumn = "yes"
opt.shiftwidth = 4
opt.ttyfast = true
opt.encoding = "utf-8"
opt.termguicolors = true
opt.background = "dark"
vim.g.blamer_enabled = true
vim.g.material_style = "deep ocean"
vim.g.neovide_transparency = 0.9
vim.g.neovide_cursor_animation_length = 0.05
vim.g.neovide_cursor_trail_size = 0.05
vim.g.neovide_refresh_rate = 140

vim.api.nvim_command("set fillchars+=diff:/")
-- opt.fillchars += "diff:/"

opt.splitbelow = true
opt.splitright = true

opt.foldmethod = "expr"
opt.foldexpr = "nvim_treesitter#foldexpr()" -- use treesitter structure for folds
opt.foldlevelstart = 20

opt.hidden = true -- Enable background buffers
opt.history = 100 -- Remember N lines in history
opt.lazyredraw = true -- Faster scrolling
opt.synmaxcol = 240 -- Max column for syntax highlight
opt.updatetime = 700 -- ms to wait for trigger an event
vim.g.do_filetype_lua = 1

-- External Configs
require("config.lazy")
require("keymappings")

require("development.git-setup")

local saga = require("lspsaga")
-- use default config
saga.setup({
	code_action_icon = "",
	show_outline = {
		auto_preview = false,
	},
})

-- TODO: Somewhat replaceable with LSP Zero
-- require("development.cmp-setup")
require("development.lsp")

require("theming.nvimtree-setup")
require("theming.aesthetics")

require("misc.telescope-settings")
-- require("misc.prose")
