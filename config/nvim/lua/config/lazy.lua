-- Bootstrap lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
	local lazyrepo = "https://github.com/folke/lazy.nvim.git"
	local out = vim.fn.system({ "git", "clone", "--filter=blob:none", "--branch=stable", lazyrepo, lazypath })
	if vim.v.shell_error ~= 0 then
		vim.api.nvim_echo({
			{ "Failed to clone lazy.nvim:\n", "ErrorMsg" },
			{ out, "WarningMsg" },
			{ "\nPress any key to exit..." },
		}, true, {})
		vim.fn.getchar()
		os.exit(1)
	end
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
	{
		"folke/tokyonight.nvim",
		lazy = false, -- make sure we load this during startup if it is your main colorscheme
		priority = 1000, -- make sure to load this before all the other start plugins
		config = function()
			require("tokyonight").setup({
				style = "night",
				transparent = "true",
				styles = {
					comments = { italic = true },
					keywords = { italic = true },
					-- Background styles. Can be "dark", "transparent" or "normal"
					-- sidebars = "dark", -- style for sidebars, see below
					-- floats = "dark", -- style for floating windows
				},
				sidebars = { "qf", "help", "packer", "DiffviewFiles" },
				on_colors = function(colors)
					colors.hint = colors.orange
					colors.error = "#ff0000"
				end,
				-- Borderless Telescope
				on_highlights = function(hl, c)
					local prompt = "#2d3149"
					hl.TelescopeNormal = {
						bg = c.bg_dark,
						fg = c.fg_dark,
					}
					hl.TelescopeBorder = {
						bg = c.bg_dark,
						fg = c.bg_dark,
					}
					hl.TelescopePromptNormal = {
						bg = prompt,
					}
					hl.TelescopePromptBorder = {
						bg = prompt,
						fg = prompt,
					}
					hl.TelescopePromptTitle = {
						bg = prompt,
						fg = prompt,
					}
					hl.TelescopePreviewTitle = {
						bg = c.bg_dark,
						fg = c.bg_dark,
					}
					hl.TelescopeResultsTitle = {
						bg = c.bg_dark,
						fg = c.bg_dark,
					}
				end,
			})
			-- load the colorscheme here
			vim.cmd([[colorscheme tokyonight-night]])
		end,
	},
	{ "neovim/nvim-lspconfig" },
	{ "williamboman/mason.nvim", lazy = false },
	{
		"williamboman/mason-lspconfig.nvim",
		dependencies = {
			"williamboman/mason.nvim",
			"neovim/nvim-lspconfig",
		},
		config = function()
			require("mason").setup()
			local mason_lspconfig = require("mason-lspconfig")
			local cap = require('blink.cmp').get_lsp_capabilities()
			mason_lspconfig.setup({
				lazy = true,
				ensure_installed = {
					"rust_analyzer",
					"lua_ls",
					"html",
					"cssls",
					"yamlls",
				},
			})
			require("lspconfig").lua_ls.setup({ capabilities = cap })
			require("lspconfig").rust_analyzer.setup({ capabilities = cap })
			require("lspconfig").html.setup({ capabilities = cap })
			require("lspconfig").cssls.setup({ capabilities = cap })
		end,
	},
	{
		"pmizio/typescript-tools.nvim",
		dependencies = { "nvim-lua/plenary.nvim", "neovim/nvim-lspconfig" },
		opts = {},
	},
	{ "folke/which-key.nvim", lazy = true },
	{
		"saghen/blink.cmp",
		dependencies = "rafamadriz/friendly-snippets",
		-- use a release tag to download pre-built binaries
		version = "*",
		opts = {
			keymap = { preset = "super-tab" },
			appearance = {
				use_nvim_cmp_as_default = true,
				nerd_font_variant = "mono",
			},
			sources = { default = { "lsp", "path", "snippets", "buffer" } }
		},
		opts_extend = { "sources.default" }
	},
	{ "nvim-tree/nvim-web-devicons", lazy = true },
	{
		"folke/ts-comments.nvim",
		opts = {},
		event = "VeryLazy",
		enabled = vim.fn.has("nvim-0.10.0") == 1,
	},
	{
		"nvim-treesitter/nvim-treesitter",
		build = ":TSUpdate",
	},
	{
		"nvim-telescope/telescope.nvim",
		tag = "0.1.5",
		requires = { "nvim-lua/plenary.nvim" },
	},
	{ "lewis6991/gitsigns.nvim", lazy = true },
	{ "MunifTanjim/nui.nvim" },
	{
		"nvim-lualine/lualine.nvim",
		dependencies = { "nvim-tree/nvim-web-devicons" },
	},
	{
		"folke/trouble.nvim",
		dependencies = { "nvim-tree/nvim-web-devicons" },
	},
	{ "numToStr/Comment.nvim", lazy = false },
	{ "rcarriga/nvim-notify" },
	{
		"folke/todo-comments.nvim",
		dependencies = { "nvim-lua/plenary.nvim" },
	},
	{ "sindrets/diffview.nvim" },
	{
		"nvim-neo-tree/neo-tree.nvim",
		branch = "v3.x",
		dependencies = {
			"nvim-lua/plenary.nvim",
			"nvim-tree/nvim-web-devicons",
			"MunifTanjim/nui.nvim",
		},
	},
	{
		"altermo/ultimate-autopair.nvim",
		event = { "InsertEnter", "CmdlineEnter" },
		branch = "v0.6", --recommended as each new version will have breaking changes
		opts = {},
	},
	{ "windwp/nvim-ts-autotag" },
	{ "nvimtools/none-ls.nvim" },
	{ "folke/zen-mode.nvim" },
	{ "echasnovski/mini.surround", version = "*" },
	{ "ggandor/lightspeed.nvim" },
	{ "glepnir/lspsaga.nvim", lazy = true, dependencies = { "neovim/nvim-lspconfig" } },
	{ "mfussenegger/nvim-jdtls" }, -- Java Toolkit

	{ "Shatur/neovim-ayu" },
	{ "tiagovla/tokyodark.nvim" },
	{ "folke/tokyonight.nvim" },
	{ "morhetz/gruvbox" },
	{ "marko-cerovac/material.nvim" },
	{ "nyoom-engineering/oxocarbon.nvim" },
	{ "thedenisnikulin/vim-cyberpunk" },
	{ "kdheepak/lazygit.nvim" },
	{ "lewis6991/gitsigns.nvim" },
	{ "petertriho/nvim-scrollbar" }, -- fancy scrollbar
	{ "lambdalisue/nerdfont.vim" },
	{ "akinsho/bufferline.nvim" },
	{ "reedes/vim-pencil" },
	{ "luukvbaal/stabilize.nvim" }, -- fixes a really annoying problem where your window jumps around when a new buffer is spawned
	{ "nvim-treesitter/nvim-treesitter-textobjects" },
	{
		"nvimdev/dashboard-nvim",
		event = "VimEnter",
		config = function()
			require("dashboard").setup()
		end,
		dependencies = { { "nvim-tree/nvim-web-devicons" } },
	},
})
