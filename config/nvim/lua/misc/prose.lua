--=====================
-- Prose/Documents Setup
--=====================

local auto = vim.api.nvim_create_autocmd

-- Vim Pencil
vim.api.nvim_command("let g:pencil#wrapModeDefault = 'soft'")

local pencil = vim.api.nvim_create_augroup("pencil", {clear = true})
auto({"FileType"}, {pattern = {"markdown"}, command = "call pencil#init()", group = pencil})
auto({"FileType"}, {pattern = {"text"}, command = "call pencil#init()", group = pencil})
