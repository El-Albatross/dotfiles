----------------------------------
-- Vimwiki + Zettelkasten Settings
----------------------------------

vim.g.vimwiki_use_mouse = 1
vim.g.vimwiki_markdown_link_ext = 1
vim.g.vimwiki_auto_chdir = 1
vim.g.vimwiki_global_ext = 0
vim.g.vimwiki_use_calendar = 1

vim.g.vimwiki_list = {
	{
		path = '~/zettel',
		syntax = 'markdown',
		ext = '.md'
	},
	{
		path = '~/vimwiki',
		syntax = 'markdown',
		ext = '.md',
		path_html = '~/vimwiki/html/',
		custom_wiki2html = 'vimwiki_markdown',
		auto_tags = 1,
		auto_toc = 1
	},
}

vim.g.calendar_options = 'nornu'

-- zettel
vim.g.zettel_fzf_command = "rg --column --line-number --ignore-case --no-heading --color=always"
