-- Language Server Protocol Settings

require("null-ls").setup {
	sources = {
		require("null-ls").builtins.formatting.stylua,
		require("null-ls").builtins.formatting.ktlint,
		-- require("null-ls").builtins.diagnostics.eslint_d,
		require("null-ls").builtins.formatting.prettierd,
		-- require("null-ls").builtins.completion.luasnip,
		require("null-ls").builtins.diagnostics.statix
	},
}

-- Typescript Spacing
vim.api.nvim_create_autocmd(
	{ "FileType" },
	{ pattern = { "typescript", "typescriptreact", "javascript" }, command = "set tabstop=2" }
)
vim.api.nvim_create_autocmd(
	{ "FileType" },
	{ pattern = { "typescript", "typescriptreact", "javascript" }, command = "set shiftwidth=2" }
)
