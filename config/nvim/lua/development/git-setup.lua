-- gitsigns
require('gitsigns').setup{
   on_attach = function(bufnr)
       local gs = package.loaded.gitsigns

       local function map(mode, l, r, opts)
         opts = opts or {}
         opts.buffer = bufnr
         vim.keymap.set(mode, l, r, opts)
       end

       -- Navigation
       map('n', ']c', "&diff ? ']c' : '<cmd>Gitsigns next_hunk<CR>'", {expr=true})
       map('n', '[c', "&diff ? '[c' : '<cmd>Gitsigns prev_hunk<CR>'", {expr=true})

       -- Actions
       map({'n', 'v'}, '<space>hs', ':Gitsigns stage_hunk<CR>')
       map({'n', 'v'}, '<space>hr', ':Gitsigns reset_hunk<CR>')
       map('n', '<space>hS', gs.stage_buffer)
       map('n', '<space>hu', gs.undo_stage_hunk)
       map('n', '<space>hR', gs.reset_buffer)
       map('n', '<space>hp', gs.preview_hunk)
       map('n', '<space>hb', function() gs.blame_line{full=true} end)
       map('n', '<space>tb', gs.toggle_current_line_blame)
       map('n', '<space>hd', gs.diffthis)
       map('n', '<space>hD', function() gs.diffthis('~') end)
       map('n', '<space>td', gs.toggle_deleted)

       -- Text object
       map({'o', 'x'}, 'ih', ':<C-U>Gitsigns select_hunk<CR>')
     end
}

