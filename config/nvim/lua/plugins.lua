local fn = vim.fn
local install_path = fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"

if fn.empty(fn.glob(install_path)) > 0 then
	packer_bootstrap =
		fn.system({ "git", "clone", "--depth", "1", "https://github.com/wbthomason/packer.nvim", install_path })
	vim.cmd([[packadd packer.nvim]])
end

return require("packer").startup(function(use)
	-- Packer
	use("wbthomason/packer.nvim")

	-- Neovim Essentials
	use("nvim-lua/popup.nvim")
	use("nvim-lua/plenary.nvim")
	use("nvim-telescope/telescope.nvim")
	use("nvim-neotest/nvim-nio")
	use("nvim-telescope/telescope-fzy-native.nvim")
	use("MunifTanjim/nui.nvim") -- UI toolkit
	use({ "nvim-neo-tree/neo-tree.nvim", branch = "v2.x" }) -- better file explorer
	use("nvim-treesitter/nvim-treesitter")
	use("nvim-treesitter/nvim-treesitter-textobjects")
	use("ahmedkhalf/project.nvim") -- project management
	use({
		"ggandor/lightspeed.nvim",
		config = function()
			require("lightspeed").setup({})
		end,
	}) -- Extra fast motions

	-- Native LSP Stuff
	use("williamboman/mason.nvim") -- LSP installer
	use("williamboman/mason-lspconfig.nvim") -- LSP configurer
	use("neovim/nvim-lspconfig")
	use("mfussenegger/nvim-dap") -- debugger
	-- use("rcarriga/nvim-dap-ui") -- debugger (dap) UI
	use("hrsh7th/nvim-cmp") -- autocomplete engine
	use("onsails/lspkind-nvim") -- Icons for lsp completions
	use("L3MON4D3/LuaSnip") -- snippet engine
	use("rafamadriz/friendly-snippets") -- list of snippets
	use("glepnir/lspsaga.nvim") -- Big LSP utility
	use("mfussenegger/nvim-jdtls") -- Java Toolkit
	use("jose-elias-alvarez/null-ls.nvim")
	use("nanotee/sqls.nvim") -- SQL toolkit

	-- Useful Utilities
	use("folke/trouble.nvim") -- For quick diagnostic jumping
	use("folke/todo-comments.nvim") -- Marks comments as TODO, can search for them with Trouble
	use("b3nj5m1n/kommentary") -- easy comment manipulation
	use("folke/which-key.nvim") -- keymapping document generator

	use({
		"luukvbaal/stabilize.nvim", -- fixes a really annoying problem where your window jumps around when a new buffer is spawned
		config = function()
			require("stabilize").setup({})
		end,
	})

	-- A E S T H E T I C S
	-- colorschemes
	use("Shatur/neovim-ayu")
	use("tiagovla/tokyodark.nvim")
	use("folke/tokyonight.nvim")
	use("morhetz/gruvbox")
	use("marko-cerovac/material.nvim")
	use({ "shaunsingh/oxocarbon.nvim", run = "./install.sh" })
	use("thedenisnikulin/vim-cyberpunk")
	use("shortcuts/no-neck-pain.nvim")

	-- UI Enhancements
	use("glepnir/dashboard-nvim") -- startup dashboard
	use("hoob3rt/lualine.nvim") -- statusline
	use("kyazdani42/nvim-web-devicons") -- icons
	use("akinsho/nvim-bufferline.lua") -- top buffer line
	use({
		"norcalli/nvim-colorizer.lua", -- colors hexcodes
		config = function()
			require("colorizer").setup(nil, { css = true })
		end,
	})
	use("petertriho/nvim-scrollbar") -- fancy scrollbar
	use("lambdalisue/nerdfont.vim")

	-- Git tools
	use("sindrets/diffview.nvim") -- git diff viewer
	use("lewis6991/gitsigns.nvim") -- the one git utility I actually use all the time

	-- Replace with Lua/other stuff eventually
	use({ "junegunn/fzf", run = "./install --bin" })
	use("junegunn/fzf.vim")

	use("reedes/vim-pencil") -- better line wrapping for text/markdown files
	if packer_bootstrap then
		require("packer").sync()
	end
end)
