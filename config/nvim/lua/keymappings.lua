--------------------------------
--      KeyBinds
--------------------------------
local map = function(mode, lhs, rhs, opts)
	local options = { noremap = true, silent = true }
	if opts then
		options = vim.tbl_extend("force", options, opts)
	end
	vim.keymap.set(mode, lhs, rhs, options)
end

local function n(key, action)
	map('n', key, action)
end

-- Buffer Switching n("<leader>;", ":bnext<cr>")
n("<leader>;", ":bnext<cr>")
n("<leader>:", ":bprev<cr>")

-- Copy selected text to system clipboard (requires gvim/nvim/vim-x11 installed):
n("<C-c", "\"y")
n("<C-p>", "\"+P")

n("<C-h>", "<C-w>h")
n("<C-l>", "<C-w>l")
n("<C-j>", "<C-w>j")
n("<C-k>", "<C-w>k")

-- Center Search Results
n("n", "nzz")
n("N", "Nzz")
n("*", "*zz")
n("#", "#zz")
n("g*", "g*zz")

-- Replace all
n("R", ":%s//g<Left><Left>")

-- LightSpeed Omni-mode
n("s", "<Plug>Lightspeed_omni_s")
n("gs", "<Plug>Lightspeed_omni_gs")

-- Move lines/chunks up and down
n("<A-j>", ":m .+1<CR>==")
n("<A-k>", ":m .-2<CR>==")
n("<A-j>", ":m '>+1<CR>gv=gv")
n("<A-j>", ":m '<-2<CR>gv=gv")

-- Trouble
n("<leader>xx", "<cmd>TroubleToggle<cr>")
n("<leader>xd", "<cmd>TodoTrouble<cr>")

-- nnoremap <leader>gs :G<cr>
n("<leader>gs", ":LazyGit<cr>")
n("<leader>gt", ":Gitsigns setqflist<cr>")
-- more git commands are in git-setup.lua

-- Undotree
n("<F3>", ":UndotreeToggle<CR>")

-- NvimTreeToggle
n("<C-n>", ":Neotree show toggle<CR>")

local telescope = require('telescope.builtin')

-- NoNeckPain
n("<leader>ff", ":ZenMode<cr>")

-- Telescope
n("<leader>sf",  ":Telescope find_files<CR>")
n("<leader>sg",  ":Telescope live_grep theme=ivy<CR>")
n("<leader>sb",  ":Telescope buffers theme=ivy<CR>")
n("<leader>sh",  ":Telescope help_tags theme=ivy<cr>")
n("<leader>sr",  ":Telescope registers theme=ivy<cr>")
n("<leader>sj",  ":Telescope jumplist theme=ivy<cr>")
n("<leader>st",  ":Telescope treesitter theme=ivy<cr>")
n("<leader>sp",  ":Telescope projects<cr>")
n("<leader>sw",  ":Telescope lsp_dynamic_workspace_symbols theme=ivy<cr>")
n("<leader>ss",  ":Telescope lsp_document_symbols theme=ivy<cr>")

n("<leader>tc", telescope.colorscheme)
n("<leader>fh", telescope.oldfiles)
n("<leader>sm", telescope.marks)
n("<leader>gr", ":Telescope lsp_references theme=cursor<cr>")

local lsp = vim.lsp.buf

-- LSP Saga
n("gh",         "lsp_finder<cr>")
n("<leader>rn", "<cmd>Lspsaga rename<cr>")
n("gs",         lsp.signature_help)
n("<leader>ca", "<cmd>Lspsaga code_action<cr>")
map("x", "gx",  ":<c-u>Lspsaga range_code_action<cr>")
n("K",          "<cmd>Lspsaga hover_doc<cr>")
n("gp",         "<cmd>Lspsaga peek_definition<cr>")
n("go",         "<cmd>Lspsaga show_line_diagnostics<cr>")
n("gj",         "<cmd>Lspsaga diagnostic_jump_next<cr>")
n("gk",         "<cmd>Lspsaga diagnostic_jump_prev<cr>")
n("<C-u>",      "<cmd>lua require('lspsaga.action').smart_scroll_with_saga(-1, '<c-u>')<cr>")
n("<C-d>",      "<cmd>lua require('lspsaga.action').smart_scroll_with_saga(1, '<c-d>')<cr>")
n("<leader>v", 	":Lspsaga outline toggle<CR>")

-- Lsp Keymaps
n('gD',         lsp.declaration)
n('gd', 		lsp.definition)
n('gi', 		lsp.implementation)
n('<leader>wa', lsp.add_workspace_folder)
n('<leader>wr', lsp.remove_workspace_folder)
n('<leader>D', 	lsp.type_definition)
n('<leader>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>')
n('<leader>fo', '<cmd>lua vim.lsp.buf.format({async=true})<CR>')

-- Bufferline
n("<space>1", "<cmd>BufferLineGoToBuffer 1<CR>")
n("<space>2", "<cmd>BufferLineGoToBuffer 2<CR>")
n("<space>3", "<cmd>BufferLineGoToBuffer 3<CR>")
n("<space>4", "<cmd>BufferLineGoToBuffer 4<CR>")
n("<space>5", "<cmd>BufferLineGoToBuffer 5<CR>")
n("<space>6", "<cmd>BufferLineGoToBuffer 6<CR>")
n("<space>7", "<cmd>BufferLineGoToBuffer 7<CR>")
n("<space>8", "<cmd>BufferLineGoToBuffer 8<CR>")
n("<space>9", "<cmd>BufferLineGoToBuffer 9<CR>")
