-- AESTHETICS
-- Colors
local opt = vim.opt

require("lualine").setup({
	options = {
		theme = "auto",
	},
	sections = {
		lualine_y = { 'os.date("%I:%M:%S", os.time())' },
	},
})

-- Lualine
-- Pick between the preset themes or a custom theme from below
-- require("theming/bubble-lualine")
-- require('theming/evil-lualine')

require("bufferline").setup({
	options = {
		view = "multiwindow",
		numbers = "ordinal",
		modified_icon = "●",
		tab_size = 18,
		diagnostics = "nvim_diagnostics",
		separator_style = { "|", "" },
	},
})

require("scrollbar").setup()
