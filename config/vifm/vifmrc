" vim: filetype=vim

source ~/.config/vifm/vifmshortcuts
" {{{ General config
" This is the actual command used to start vi.  The default is vim.
" If you would like to use another vi clone such as Elvis or Vile
" you will need to change this setting.
set vicmd=nvim

set vimhelp

" set vimhelp true
" This makes vifm perform file operations on its own instead of relying on
" standard utilities like `cp`.  While using `cp` and alike is a more universal
" solution, it's also much slower when processing large amounts of files and
" doesn't support progress measuring.
set syscalls

" Open with preview window
view

" Trash Directory
" The default is to move files that are deleted with dd or :d to
" the trash directory.  If you change this you will not be able to move
" files by deleting them and then using p to put the file in the new location.
" I recommend not changing this until you are familiar with vifm.
" This probably shouldn't be an option.
set trash

set previewoptions+=graphicsdelay:0
set previewoptions+=hardgraphicsclear

" This is how many directories to store in the directory history.
set history=1000

" Automatically resolve symbolic links on l or Enter.
set nofollowlinks

" Natural sort of (version) numbers within text.
set sortnumbers

" Maximum number of changes that can be undone.
set undolevels=100

" If you would like to run an executable file when you
" press return on the file name set this.
set norunexec

" Selected color scheme
colorscheme palenight
" colorscheme near-default

" Format for displaying time in file list. For example:
" TIME_STAMP_FORMAT=%m/%d-%H:%M
" See man date or man strftime for details.
set timefmt=%m/%d\ %H:%M

" Show list of matches on tab completion in command-line mode
set wildmenu

" Display completions in a form of popup with descriptions of the matches
set wildstyle=popup

" Display suggestions in normal, visual and view modes for keys, marks and
" registers (at most 5 files).  In other view, when available.
set suggestoptions=normal,visual,view,otherpane,keys,marks,registers

" Ignore case in search patterns unless it contains at least one uppercase
" letter
set ignorecase
set smartcase

" Don't highlight search results automatically
set nohlsearch

" Use increment searching (search while typing)
set incsearch

" Try to leave some space from cursor to upper/lower border in lists
set scrolloff=4

" Don't do too many requests to slow file systems
set slowfs=curlftpfs

" Things that should be stored in vifminfo
set vifminfo=dhistory,chistory,state,shistory,phistory,fhistory,dirstack,registers,bookmarks,bmarks

" Dont show delete confirmation
set confirm-=delete

" Fuse
set fusehome="~/VAULT"

" ------------------------------------------------------------------------------

" :com[mand][!] command_name action
" The following macros can be used in a command
" %a is replaced with the user arguments.
" %c the current file under the cursor.
" %C the current file under the cursor in the other directory.
" %f the current selected file, or files.
" %F the current selected file, or files in the other directory.
" %b same as %f %F.
" %d the current directory name.
" %D the other window directory name.
" %m run the command in a menu window

command! df df -h %m 2> /dev/null
command! diff nvim -d %f %F
command! zip zip -r %f.zip %f
command! run !! ./%f
command! make !!make %a
command! mkcd :mkdir %a | cd %a
command! vgrep vim "+grep %a"
command! reload :write | restart

" Empty the ruler. By default, it shows the number of directories+files.
set rulerformat=
" }}}


" {{{ File preview & file opening
" The file type is for the default programs to be used with
" a file extension.
" :filetype pattern1,pattern2 defaultprogram,program2
" :fileviewer pattern1,pattern2 consoleviewer
" The other programs for the file type can be accessed with the :file command
" The command macros %f, %F, %d, %F may be used in the commands.
" The %a macro is ignored.  To use a % you must put %%.

" For automated FUSE mounts, you must register an extension with :file[x]type
" in one of following formats:
"
" :filetype extensions FUSE_MOUNT|some_mount_command using %SOURCE_FILE and %DESTINATION_DIR variables
" %SOURCE_FILE and %DESTINATION_DIR are filled in by vifm at runtime.
" A sample line might look like this:
" :filetype *.zip,*.jar,*.war,*.ear FUSE_MOUNT|fuse-zip %SOURCE_FILE %DESTINATION_DIR
"
" :filetype extensions FUSE_MOUNT2|some_mount_command using %PARAM and %DESTINATION_DIR variables
" %PARAM and %DESTINATION_DIR are filled in by vifm at runtime.
" A sample line might look like this:
" :filetype *.ssh FUSE_MOUNT2|sshfs %PARAM %DESTINATION_DIR
" %PARAM value is filled from the first line of file (whole line).
" Example first line for SshMount filetype: root@127.0.0.1:/
"
" You can also add %CLEAR if you want to clear screen before running FUSE
" program.


" CSV/Excel
filetype *.csv,*.xlsx vd %f

" fileviewer *.csv sed "s/,,,,/,,-,,/g;s/,,/ /g" %c | column -t | sed "s/ - /  /g" | cut -c -%pw
filetype *.json nvim
fileviewer *.json bat --color=always --plain --wrap never --pager never %c -p

" VimWiki
filetype *.wiki nvim
fileviewer *.wiki bat --plain --wrap never --pager never %c -p

" HTMLs
fileviewer *.html w3m -dump %c
filextype *.html,*.htm nvim %c
" 2>/dev/null &

" FTP with curlftpfs
filetype *.ftp
        \ {Mount with CurlFtpFS}
        \ FUSE_MOUNT2|curlftpfs -o ftp_port=-,,disable_eprt %PARAM %DESTINATION_DIR %FOREGROUND,

" SSHFS
filetype *.ssh
        \ {Mount with SSHFS}
        \ FUSE_MOUNT2|sshfs %PARAM %DESTINATION_DIR %FOREGROUND,

" Text based files
filetype <text/*> nvim
fileviewer <text/*> bat --color always --wrap never --pager never %c -p

filetype *.yml,*.tml,*.yaml nvim
fileviewer *.yml,*.tml,*.yaml bat --color=always --wrap never --pager never %c -p


" Jupyter Notebooks
filetype *.ipynb nvim


" PDFs
filextype *.pdf zathura %c %i &
fileviewer *.pdf pdftotext -nopgbrk %c -

" ePUBs
" filextype *.epub zathura %c %i &
fileviewer *.epub
        \ vifmimg epub %px %py %pw %ph %c
        \ %pc
        \ vifmimg clear

filetype *.epub,*.mobi,*.azw3 epy

" DOCX, ODT
filetype *.docx,*.odt libreoffice --writer

" Fonts
fileviewer *.otf,*.ttf,*.woff
        \ vifmimg font %px %py %pw %ph %c
        \ %pc
        \ vifmimg clear

" Audios
filetype <audio/*> mpv %c %i </dev/null &>/dev/null &
fileviewer <audio/*>
        \ vifmimg audio %px %py %pw %ph %c
        \ %pc
        \ vifmimg clear

" Videos
filetype <video/*> mpv %c %i </dev/null &>/dev/null &
fileviewer <video/*>
        \ vifmimg video %px %py %pw %ph %c
        \ %pc
        \ vifmimg clear

" Images
filextype <image/*> sxiv </dev/null &>/dev/null &
fileviewer <image/*>
		\ wezterm imgcat --width %pw --height %ph %c:p %pd

" CAD
filetype *.scad openscad %f


" Archives
fileviewer *.zip,*.jar,*.war,*.ear,*.oxt zip -sf %c
fileviewer *.tgz,*.tar.gz tar -tzf %c
fileviewer *.tar.bz2,*.tbz2 tar -tjf %c
fileviewer *.tar.txz,*.txz xz --list %c
fileviewer *.tar tar -tf %c
fileviewer *.rar unrar v %c
fileviewer *.7z 7z l %c

" Dont show preview on ../ as this confuses me at times
fileview ../ echo >/dev/null

" Show ls in the preview window, it creates a similar look as ranger.
" The default directory tree thing is really messy
fileviewer */ ls --color --group-directories-first
fileviewer .*/ ls --color --group-directories-first

" Other files
" Using xdg-open to open the highlighted file with a compatible program and
" the reason why I am using "file" to preview other files is so that "vifm"
" does not lag when trying "cat" the file
filetype * mimeopen -a %c
fileviewer * file -b %c
" }}}


"{{{ Key mappings
" Easily quit vifm by hitting q
nmap q ZQ

" Selection
nnoremap <Space> t<Down>

" Use semicolon to enter command mode
nnoremap ; :

" Diffview comparison with Neovim
nnoremap cv :diff<cr>

" Set highlighted image as wallpaper

" Upload highlighted file to 0x0.st and then save url to clipboard
nnoremap 0x0 :!curl -s -F'file=@%c' https://0x0.st > /dev/null | xclip -sel clip && notify-send "vifm" "File uploaded: $(xclip -o -selection clipboard)" &<cr>

" Reverse image search with Tiney
nnoremap re :!bash ~/bin/utils/tineye %c &<cr>

" Go to the file that is right before "../" for going to the top most file
nnoremap gg ggj

" Start shell in current directory
nnoremap s :shell<cr>

" Display sorting dialog
nnoremap S :sort<cr>

" Toggle visibility of preview window
nnoremap w :view<cr>
vnoremap w :view<cr>gv

" Open file in nvim
nnoremap o :!nvim %f<cr>

" Open file in the background using its default program
nnoremap gb :file &<cr>l

" Yank current directory path into the clipboard
nnoremap yd :!echo %d | xclip -i -selection clipboard %i<cr>

" Yank current file path into the clipboard
nnoremap yf :!echo %c:p | xclip -i -selection clipboard %i<cr>

" Edit Vimrc and quick reload
nnoremap ,c :write | edit $MYVIFMRC | restart<cr>

" Mappings for faster renaming
nnoremap I cw<c-a>
nnoremap cc cw<c-u>
nnoremap A cw

" Extract an archive
nnoremap x :!/home/gabe/.scripts/tools/extract %f &<cr>

" Make a new directory
nnoremap mkd :mkdir<space>

nnoremap mo :!dragon %f </dev/null &>/dev/null & <cr>

" Set new wallpaper

nnoremap bgd :!nitrogen --save --set-zoom-fill %f <cr>
nnoremap bgs :!nitrogen --save --head=0 --set-zoom-fill %f && nitrogen --save --head=1 --set-zoom-fill %f<cr>

" Switch lockscreen wallpaper to selection

nnoremap bl :!betterlockscreen -u %f <cr>

" tmux
screen!
"}}}


"{{{ Icons
" file types
set classify='  :dir:/,  :exe:,  :reg:,  :link:'
" various file names
set classify+='  ::../::,  ::*.sh::,  ::*.[hc]pp::,  ::*.[hc]::,  ::/^copying|license$/::,  ::.git/,,*.git/::,  ::*.epub,,*.fb2,,*.djvu::,  ::*.pdf::,  ::*.htm,,*.html,,**.[sx]html,,*.xml::'
" archives
set classify+='  ::*.7z,,*.ace,,*.arj,,*.bz2,,*.cpio,,*.deb,,*.dz,,*.gz,,*.jar,,*.lzh,,*.lzma,,*.rar,,*.rpm,,*.rz,,*.tar,,*.taz,,*.tb2,,*.tbz,,*.tbz2,,*.tgz,,*.tlz,,*.trz,,*.txz,,*.tz,,*.tz2,,*.xz,,*.z,,*.zip,,*.zoo::'
" images
set classify+='  ::*.bmp,,*.gif,,*.jpeg,,*.jpg,,*.ico,,*.png,,*.ppm,,*.svg,,*.svgz,,*.tga,,*.tif,,*.tiff,,*.xbm,,*.xcf,,*.xpm,,*.xspf,,*.xwd::'
" audio
set classify+='  ::*.aac,,*.anx,,*.asf,,*.au,,*.axa,,*.flac,,*.m2a,,*.m4a,,*.mid,,*.midi,,*.mp3,,*.mpc,,*.oga,,*.ogg,,*.ogx,,*.ra,,*.ram,,*.rm,,*.spx,,*.wav,,*.wma,,*.ac3::'
" media
set classify+='  ::*.avi,,*.ts,,*.axv,,*.divx,,*.m2v,,*.m4p,,*.m4v,,.mka,,*.mkv,,*.mov,,*.mp4,,*.flv,,*.mp4v,,*.mpeg,,*.mpg,,*.nuv,,*.ogv,,*.pbm,,*.pgm,,*.qt,,*.vob,,*.wmv,,*.xvid::'
" office files
set classify+='  ::*.doc,,*.docx::,  ::*.xls,,*.xls[mx]::,  ::*.pptx,,*.ppt::'
"}}}

